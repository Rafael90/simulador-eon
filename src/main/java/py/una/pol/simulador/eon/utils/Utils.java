/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package py.una.pol.simulador.eon.utils;

import Metricas.Metricas;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.SimpleWeightedGraph;
import py.una.pol.simulador.eon.models.AssignFsResponse;
import py.una.pol.simulador.eon.models.Copia;
import py.una.pol.simulador.eon.models.Core;
import py.una.pol.simulador.eon.models.Demand;
import py.una.pol.simulador.eon.models.EstablishedRoute;
import py.una.pol.simulador.eon.models.FrequencySlot;
import py.una.pol.simulador.eon.models.Link;
import py.una.pol.simulador.eon.models.enums.TopologiesEnum;
import py.una.pol.simulador.eon.rsa.Algorithms;
import static py.una.pol.simulador.eon.utils.Cromosoma.ordenarRutas;

/**
 * Utilerías generales
 *
 * @author Néstor E. Reinoso Wood
 */
public class Utils {

    /**
     * Creates the graph that represents the optical network
     *
     * @param topology Topology selected for the network
     * @param numberOfCores Quantity of cores in each link
     * @param fsWidth Width of the frequency slots
     * @param capacity Quantity of frequency slots in a core
     * @return Graph that represents the optical network
     * @throws IOException Error de I/O
     * @throws IllegalArgumentException Parámetros no válidos
     */
    public static Graph<Integer, Link> createTopology(TopologiesEnum topology, int numberOfCores,
            BigDecimal fsWidth, Integer capacity)
            throws IOException, IllegalArgumentException {

        ObjectMapper objectMapper = new ObjectMapper();
        Graph<Integer, Link> g = new SimpleWeightedGraph<>(Link.class);
        InputStream is = ResourceReader.getFileFromResourceAsStream(topology.filePath());
        JsonNode object = objectMapper.readTree(is);

        for (int i = 0; i < object.get("network").size(); i++) {
            g.addVertex(i);
        }
        int vertex = 0;
        for (JsonNode node : object.get("network")) {
            for (int i = 0; i < node.get("connections").size(); i++) {
                int connection = node.get("connections").get(i).intValue();
                int distance = node.get("distance").get(i).intValue();
                List<Core> cores = new ArrayList<>();

                for (int j = 0; j < numberOfCores; j++) {
                    Core core = new Core(fsWidth, capacity);
                    cores.add(core);
                }

                Link link = new Link(distance, cores, vertex, connection);
                g.addEdge(vertex, connection, link);
                g.setEdgeWeight(link, distance);
            }
            vertex++;
        }
        return g;
    }

    /**
     * Genera una lista de demandas en base a los argumentos de entrada
     *
     * @param lambda Cantidad de demandas a insertar por unidad de tiempo
     * @param totalTime Tiempo total de simulación
     * @param fsMin Cantidad mínima de ranuras que puede ocupar una demanda
     * @param fsMax Cantidad máxima de ranuras que puede ocupar una demanda
     * @param cantNodos Cantidad de nodos de la red
     * @param HT Erlang/Lambda
     * @param demandId Identificador de la última demanda generada
     * @param insertionTime Tiempo de inserción de las demanda
     * @return Lista de demandas generadas
     */
    public static List<Demand> generateDemands(Integer lambda, Integer totalTime,
            Integer fsMin, Integer fsMax, Integer cantNodos, Integer HT, Integer demandId, Integer insertionTime) {
        List<Demand> demands = new ArrayList<>();
        Random rand;
        Integer demandasQuantity = MathUtils.poisson(lambda);
        for (Integer j = demandId; j < demandasQuantity + demandId; j++) {
            rand = new Random();
            Integer source = rand.nextInt(cantNodos);
            Integer destination = rand.nextInt(cantNodos);
            Integer fs = (int) (Math.random() * (fsMax - fsMin + 1)) + fsMin;
            while (source.equals(destination)) {
                destination = rand.nextInt(cantNodos);
            }
            Integer tLife = MathUtils.getLifetime(HT);
            demands.add(new Demand(j, source, destination, fs, tLife, false, insertionTime));
        }
        return demands;
    }

    /**
     * Calcula el valor de Crosstalk en un núcleo
     *
     * @param n Número de cores vecinos
     * @param h Crosstalk por Unidad de Longitud
     * @param L Longitud del enlace
     * @return Crosstalk
     */
    public static double XT(int n, double h, int L) {
        double XT = 0;
        for (int i = 0; i < n; i++) {
            XT = XT + (h * (L * 1000));
        }
        return XT;
    }

    /**
     * Calcula la cantidad de nucleos adyacentes para un núcleo en una red de 7
     * núcleos
     *
     * @param core Núcleo a utilizar para encontrar la cantidad de vecinos
     * @return Cantidad de vecinos del núcleo
     */
    public static int getCantidadVecinos(int core) {
        if (core == 6) {
            return 6;
        }
        return 3;
    }

    /**
     * Conversión a decibelios
     *
     * @param value Valor de crosstalk
     * @return Valor de crosstalk en decibelios
     */
    public static BigDecimal toDB(double value) {
        try {
            //return new BigDecimal(10D*Math.log10(value));
        } catch (Exception ex) {
            return BigDecimal.ZERO;
        }
        return new BigDecimal(value);
    }

    /**
     * Función de asignación de conexiones a la red
     *
     * @param graph Red
     * @param establishedRoute Ruta a establecer
     * @param crosstalkPerUnitLength Crosstalk por unidad de distancia de la
     * fibra
     * @return Respuesta de la operación
     */
    public static AssignFsResponse assignFs(Graph<Integer, Link> graph, EstablishedRoute establishedRoute, Double crosstalkPerUnitLength) {
        for (int j = 0; j < establishedRoute.getPath().size(); j++) {
            for (int i = establishedRoute.getFsIndexBegin(); i < establishedRoute.getFsIndexBegin() + establishedRoute.getFsWidth(); i++) {
                establishedRoute.getPath().get(j).getCores().get(establishedRoute.getPathCores().get(j)).getFrequencySlots().get(i).setFree(false);
               //voy a usar en bfr  getEdgeList()
                //FrequencySlot fs= establishedRoute.getPath().get(j).getCores().get(establishedRoute.getPathCores().get(j)).getFrequencySlots().get(i);
                
                Integer core = establishedRoute.getPathCores().get(j);
                establishedRoute.getPath().get(j).getCores().get(core).getFrequencySlots().get(i).setLifetime(establishedRoute.getLifetime());
                List<Integer> coreVecinos = getCoreVecinos(core);
                // TODO: Asignar crosstalk
                for (Integer coreIndex = 0; coreIndex < establishedRoute.getPath().get(j).getCores().size(); coreIndex++) {
                    if (!core.equals(coreIndex) && coreVecinos.contains(coreIndex)) {
                        double crosstalk = XT(getCantidadVecinos(coreIndex), crosstalkPerUnitLength, establishedRoute.getPath().get(j).getDistance());
                        BigDecimal crosstalkDB = toDB(crosstalk);
                        establishedRoute.getPath().get(j).getCores().get(coreIndex).getFrequencySlots().get(i).setCrosstalk(establishedRoute.getPath().get(j).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk().add(crosstalkDB));

                        BigDecimal existingCrosstalk = graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk();
                        graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).setCrosstalk(existingCrosstalk.add(crosstalkDB));
                        //System.out.println("CT despues de suma" + graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk());
                    }
                }
            }
        }
        AssignFsResponse response = new AssignFsResponse(graph, establishedRoute);
        return response;
    }

    /**
     * Función de desasignación de conexiones a la red
     *
     * @param graph Red
     * @param establishedRoute Ruta a establecer
     * @param crosstalkPerUnitLength Crosstalk por unidad de distancia de la
     * fibra
     */
    public static void deallocateFs(Graph<Integer, Link> graph, EstablishedRoute establishedRoute, Double crosstalkPerUnitLength) {
        for (int j = 0; j < establishedRoute.getPath().size(); j++) {
            for (int i = establishedRoute.getFsIndexBegin(); i < establishedRoute.getFsIndexBegin() + establishedRoute.getFsWidth(); i++) {
                Integer core = establishedRoute.getPathCores().get(j);
                establishedRoute.getPath().get(j).getCores().get(core).getFrequencySlots().get(i).setFree(true);
                establishedRoute.getPath().get(j).getCores().get(core).getFrequencySlots().get(i).setLifetime(0);
                List<Integer> coreVecinos = getCoreVecinos(core);
                // TODO: Desasignar crosttalk
                for (Integer coreIndex = 0; coreIndex < establishedRoute.getPath().get(j).getCores().size(); coreIndex++) {
                    if (!core.equals(coreIndex) && coreVecinos.contains(coreIndex)) {
                        double crosstalk = XT(getCantidadVecinos(coreIndex), crosstalkPerUnitLength, establishedRoute.getPath().get(j).getDistance());
                        BigDecimal crosstalkDB = toDB(crosstalk);
                        establishedRoute.getPath().get(j).getCores().get(coreIndex).getFrequencySlots().get(i).setCrosstalk(establishedRoute.getPath().get(j).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk().subtract(crosstalkDB));

                        BigDecimal existingCrosstalk = graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk();
                        graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).setCrosstalk(existingCrosstalk.subtract(crosstalkDB));
                        //System.out.println("CT despues de suma" + graph.getEdge(establishedRoute.getPath().get(j).getTo(), establishedRoute.getPath().get(j).getFrom()).getCores().get(coreIndex).getFrequencySlots().get(i).getCrosstalk());
                    }
                }
            }
        }
    }

    /**
     * Obtiene los índices de los núcleos vecinos para un núcleo de la fibra
     *
     * @param coreActual Núcleo de la fibra
     * @return Núcleos adyacentes al núcleo actual
     */
    public static List<Integer> getCoreVecinos(Integer coreActual) {
        List<Integer> vecinos = new ArrayList<>();
        switch (coreActual) {
            case 0 -> {
                vecinos.add(1);
                vecinos.add(5);
                vecinos.add(6);
            }
            case 1 -> {
                vecinos.add(0);
                vecinos.add(2);
                vecinos.add(6);
            }
            case 2 -> {
                vecinos.add(1);
                vecinos.add(3);
                vecinos.add(6);
            }
            case 3 -> {
                vecinos.add(2);
                vecinos.add(4);
                vecinos.add(6);
            }
            case 4 -> {
                vecinos.add(3);
                vecinos.add(5);
                vecinos.add(6);
            }
            case 5 -> {
                vecinos.add(0);
                vecinos.add(4);
                vecinos.add(6);
            }
            case 6 -> {
                vecinos.add(0);
                vecinos.add(1);
                vecinos.add(2);
                vecinos.add(3);
                vecinos.add(4);
                vecinos.add(5);
            }
        }
        return vecinos;
    }
    
    public static boolean desfragmentacionAG( Graph<Integer, Link> topologia, String algoritmoRSAejecutar, ArrayList<EstablishedRoute> resultadosFSasignados, List<GraphPath<Integer, Link>> rutasActivas, 
        int porcentajeLongCRAG, int capacidadFSporEnlace, int tiempo, int cantIndividuos, String objetivoAG,int cantGeneracionesAG, Integer CantCores, BigDecimal MaxCrosstalk, Double crosstalkPerUnitLength, TopologiesEnum topology, BigDecimal fsWidth) throws IOException{
        
        int cantGeneraciones=0;
        double intAletorioCruce=0;
        boolean aleatoriohijoaMut;
        double aletorioMuta;
       
        Graph<Integer, Link> copiaGrafo = Utils.createTopology(topology,
                            CantCores,fsWidth , capacidadFSporEnlace);
        
        //Crear una poblacion inicial aleatoria, 50 individuos, cada individuo es un cromosoma que a su vez tiene genes que son rutas activas en la red.
        ArrayList<Cromosoma> poblacionActual = crearPoblacionInicial(topologia,algoritmoRSAejecutar,resultadosFSasignados,rutasActivas,porcentajeLongCRAG,capacidadFSporEnlace,cantIndividuos,objetivoAG,cantGeneracionesAG,CantCores, MaxCrosstalk, crosstalkPerUnitLength, topology, fsWidth );//lista de individuos o soluciones candidatas
        //Población auxiliar para crear nueva generación
        ArrayList<Cromosoma> poblacionNueva = new ArrayList<>();
        double[] probabilidad = new double[poblacionActual.size()];
        double sumatoria=0;
        
        //Padres seleccionados
         ArrayList<Cromosoma> padresAux = new ArrayList<>();
         ArrayList<Cromosoma> hijosGenerados = new ArrayList<>();
         Cromosoma aux = new Cromosoma();
         double mejorIndPobAct = 0;
         //Calculamos el tamaño de la población temporal
         int tamPoblacionTemp = poblacionActual.size()*2;
         int tamCromosoma = Math.round((rutasActivas.size()*porcentajeLongCRAG)/100);
         //Establecemos el mejor individuo para graficar en caso de que no mejore tras generaciones
         if(!poblacionActual.isEmpty()){
                 while(cantGeneraciones!=cantGeneracionesAG){
                        //Guardar los individuos en la poblacion nueva
                        copiarIndividuos(poblacionActual,poblacionNueva);
                        mejorIndPobAct = elMejorIndividuo(poblacionActual,cantGeneraciones);
                        //calcular probabilidad,un individuo con mayor porcentaje de mejora tiene mayor probabilidad de ser seleccionado
                        sumatoria=0.0;
                        for(int i=0; i<poblacionActual.size(); i++){
                            sumatoria = sumatoria+poblacionActual.get(i).getMejora();
                        }
                        for(int i=0;i<poblacionActual.size();i++){
                            probabilidad[i] = (poblacionActual.get(i).getMejora())/sumatoria;
                        }
                        //crear una nueva población 
                        int cantCruces=0;
                        while(poblacionNueva.size()<tamPoblacionTemp){
                            padresAux = seleccionarPadres(poblacionActual,probabilidad);
                            //Generamos un numero aleatorio [0,1] si es cero se hace le cruce si es 1 pasa el padre
                            Random aleatorio1 = new Random(System.currentTimeMillis());
                            intAletorioCruce = aleatorio1.nextDouble();//le pasamos el 2 porque solo queremos generar 0,1
                            //controlar mutacion con probabilidad
                            Random aleatorioMut = new Random(System.currentTimeMillis()+1);
                            Random hijoaMut = new Random(System.currentTimeMillis());
                            aletorioMuta = aleatorioMut.nextDouble();
                            aleatoriohijoaMut=hijoaMut.nextBoolean();
                            if(intAletorioCruce<=0.5){//si ocurrio el cruce
                                hijosGenerados=CrucePadres(padresAux,rutasActivas.size());
                                cantCruces = cantCruces + 1;
                                if(aletorioMuta<=0.01){//se hace o no la mutacion?0.005
                                    if(aleatoriohijoaMut){//cual de los dos se muta? 
                                        MutacionHijos(hijosGenerados.get(0),rutasActivas.size());  
                                    }else{
                                         MutacionHijos(hijosGenerados.get(1),rutasActivas.size());
                                    }
                                }
                                    for(int j=0;j<hijosGenerados.size();j++){
                                        copiarGrafo(copiaGrafo, topologia, capacidadFSporEnlace);
                                        aux=EvaluarCromosoma(hijosGenerados.get(j),rutasActivas,topologia,copiaGrafo,capacidadFSporEnlace,
                                        resultadosFSasignados,algoritmoRSAejecutar,porcentajeLongCRAG,objetivoAG,mejorIndPobAct,
                                        tamCromosoma,cantGeneraciones,tamPoblacionTemp, CantCores, MaxCrosstalk, crosstalkPerUnitLength, topology, fsWidth);
                                        if(!aux.getListaGenes().isEmpty()){//si no esta vacia,es una solucion. si esta vacia es porque hubo un bloqueo "RAFA".
                                            if(poblacionNueva.size() < tamPoblacionTemp){//si no esta llena la poblacion
                                                poblacionNueva.add(aux); //se agrega a la poblacionNueva el hijo j que paso la evaluacion "RAFA"
                                            }
                                            if(poblacionNueva.size()==tamPoblacionTemp){
                                                break;
                                            }
                                        }else if(poblacionNueva.size() < tamPoblacionTemp){ //si esta vacia el cromosoma aux hubo bloqueo entonces se agrega a la poblacionNueva al padre "RAFA".
                                            poblacionNueva.add(padresAux.get(j)); 
                                        }
                                        if(poblacionNueva.size()==tamPoblacionTemp){
                                            break;
                                        }
                                    }             
                            }
                        } 
                            //Dejar solo los 50 mejores individuos en la poblacion nueva y luego pasarlos a la poblacion actual
                            copiarMejoresIndividuos(poblacionNueva);
                            cantGeneraciones++;
                            poblacionActual.clear();
                            //poblacion nueva pasa a ser la actual
                            for(int i=0;i<poblacionNueva.size();i++){
                                poblacionActual.add(poblacionNueva.get(i));
                            }
                            poblacionNueva.clear();
                    }
                            /********Graficar la mejor solución encontrada******/
                         /*
                           if(poblacionActual.get(0).getMejora()>= 0){
                                copiarGrafo(G ,poblacionActual.get(0).getGrafoMejor(), capacidadFSporEnlace);
                                escribirArchivoDefrag(archivo, poblacionActual.get(0).getCantidadRutasMejor(), tiempo, poblacionActual.get(0).getMejora(), true ,cantGeneraciones, rutasActivas.size());
                                //Retirar resultados viejos del vector resultados, colocar los resultados de la mejor solucion           
                                for (int k=0; k<poblacionActual.get(0).getIndicesMejor().size(); k++){
                                    resultadosFSasignados.set(poblacionActual.get(0).getIndicesMejor().get(k), poblacionActual.get(0).getResultadosMejor().get(k));
                                    rutasActivas.set(poblacionActual.get(0).getIndicesMejor().get(k), poblacionActual.get(0).getRutasMejor().get(k));
                                }
                                 Calendar calendario2 = Calendar.getInstance();
                                 return true;
                            }
                           
                            }else{
                                escribirArchivoDefrag(archivo, 0, tiempo, 0, false ,cantGeneraciones, rutasActivas.size());
                                //System.out.println("La mejor solucion es : " +poblacionActual.get(0).getNroReconf());
                                return false;
                            }
                            
            }else{ 
                // System.out.println("No encontró un resultado mínimo deseado, no hace nada con el grafo. :(");
                escribirArchivoDefrag(archivo, 0, tiempo, 0, false ,cantGeneraciones, rutasActivas.size());
                            */
            }
                
        
        
            return false;    
    }
    
    public static ArrayList<Cromosoma> crearPoblacionInicial(Graph<Integer, Link> topologia, String algoritmoAejecutar, ArrayList<EstablishedRoute> resultados, List<GraphPath<Integer, Link>> rutas, int porcentajeLongCRAG, 
        int capacidadFSporEnlace, int cantIndividuos, String objetivoAG,int cantGeneracionesAG, Integer CantCores, BigDecimal MaxCrosstalk, Double crosstalkPerUnitLength, TopologiesEnum topology, BigDecimal fsWidth) throws IOException{
        
        ArrayList<Cromosoma> cromosomasList = new ArrayList<>();//lista de individuos o soluciones candidatas(poblacion inicial)
        ArrayList<Cromosoma> noDefragList = new ArrayList<>();//lista vacia en caso de que no se pueda defragmentar
        ArrayList<Integer> mejorSolucion = new ArrayList<>();//Mejor solucion del 30% de mejores buscadas
        int porcentaje = Math.round((cantIndividuos*30)/100);//30% de buenas soluciones
        int cont = 0;
        int contProb = 0;
        //va a crear el 30 porciento de la poblacion de individuos mediante el metodo de la ruleta
        
        while(cromosomasList.size()<porcentaje){
           //System.out.println("cromosomasList  : "+ cromosomasList.size());
           Cromosoma cr = new Cromosoma();
           if(!cromosomasList.isEmpty()){
            mejorSolucion=cromosomasList.get(0).getListaGenes();//es la primer solucion obtenida
           //System.out.println("mejor solucion  : "+ mejorSolucion);
           }
           if(contProb < cantGeneracionesAG*2){
            //buenasSoluciones crea un cromosoma o individuo mediante el metodo de la ruleta "RAFA"
           cr = cr.buenasSoluciones(rutas,topologia,capacidadFSporEnlace,resultados,algoritmoAejecutar,porcentajeLongCRAG,objetivoAG,cont,mejorSolucion,cont,CantCores, MaxCrosstalk, crosstalkPerUnitLength, topology,fsWidth);
           }else{
               //System.out.println("prueba  : ");
               return noDefragList;}
           if(!cr.getListaGenes().isEmpty()){
            cromosomasList.add(cr);
           }
           else{
            contProb++;
           }
           cont = cont+1;//cuenta las iteraciones
        }
//System.out.println("prueba  : ");
//System.out.println("prueba  : ");
        
        while(cromosomasList.size()<cantIndividuos){//70% de soluciones aleatorias
           Cromosoma cr = new Cromosoma();
           cr=cr.solucionesCandidatas(rutas,topologia,capacidadFSporEnlace,resultados,algoritmoAejecutar,objetivoAG, porcentajeLongCRAG, CantCores, MaxCrosstalk, crosstalkPerUnitLength, topology,fsWidth);
           // System.out.println("prueba  : "+ cromosomasList.size());
            //System.out.println("prueba 1  : ");
           if(!cr.getListaGenes().isEmpty()){
                cromosomasList.add(cr);
                cont = cont+1;

            }    
               // System.out.println("prueba  : "+ cromosomasList.size());
                
        }
        return cromosomasList;
           
    }
    
    
        //Retorna el porcentaje de mejora del mejor individuo
        //se usa en la parte de desfragmentacionAG 
        public static double elMejorIndividuo(ArrayList<Cromosoma> poblacionActual, int cantGeneraciones){
        Cromosoma auxp;
        int n = poblacionActual.size();
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (poblacionActual.get(i).getMejora() < poblacionActual.get(j).getMejora()) {
                    auxp = poblacionActual.get(i);
                    poblacionActual.set(i, poblacionActual.get(j));
                    poblacionActual.set(j, auxp);
                }
            }
        }
         System.out.println("La mejor solucion de la generacion  : " +cantGeneraciones+" - "+poblacionActual.get(0).getMejora());
            return poblacionActual.get(0).getMejora();
        }

    
        public static boolean isInList(ArrayList<Integer> lista, int elem) {
            boolean repetido = false;
            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).equals(elem)) {
                    repetido = true;
                }
            }
            return repetido;
        }
        
        
            //ver si dos resultados son iguales
        public static Boolean compararRutas(EstablishedRoute r,EstablishedRoute  r2){
            /*
            if ((r.getPath()== r2.getPath())&& (r.getFrom()==r2.getFrom()) && (r.getTo()==r2.getTo())){
                return true;
            }
            */
            int igualRuta=0;
            int igualCore=0;
            //verificada cada uno de los enlace si son iguales entre la r que es la reruteada y la anterior
           
          if(r.getPath().size()==r2.getPath().size()){  
                for(int i=0; i<r2.getPath().size();i++){
                    if(r.getPath().get(i).getFrom()==r2.getPath().get(i).getFrom()&&r.getPath().get(i).getTo()==r2.getPath().get(i).getTo()){
                        igualRuta++;
                    }    
                }
                //verificada para cada enlace su core si son iguales entre la r que es la reruteada y la anterior
                for(int j=0; j<r2.getPath().size();j++){
                    if(Objects.equals(r.getPathCores().get(j), r2.getPathCores().get(j))){
                        igualCore++;
                    }    
                }
            
            // verifica si tiene la misma cantidad de enlaces la reruteada y la anterior
            //verifica si tiene todos sus enlaces, core, from y to iguales
           // System.out.println("BLOQUEO");
                if (igualRuta==igualCore){
                    return true;
                } else {
                    return false;
                }
          }else{
             return false;
          }       
        }
    
        
        //calcula mejora AG despues de rutear con las nuevas condiciones mismo algoritmo, 
        //esta en la parte cuando se genera el 70% de la poblacion inicial y tambien se usa en el 30% ruleta  "RAFA" 
        public static double calculoMejoraAG(boolean porMsi,boolean porBfr, Graph<Integer, Link> copiaGrafo,double bfrGrafo, int capacidad, double msiGrafo, Integer CantCores){
            double resultado = 0.0;
            double msiActual = 0.0;
            double bfrActual = 0.0;
            if(porBfr){
                
                bfrActual = Metricas.BFR(copiaGrafo, capacidad, CantCores);
               // resultado = 100 - ((redondearDecimales(bfrActual, 6) * 100)/redondearDecimales(bfrGrafo, 6));
               resultado = 100 - ((redondearDecimales(bfrActual , 6) * 100)/redondearDecimales( bfrGrafo, 6));
                //System.out.println("BLOQUEO");
            }
               /*
            if(porMsi){
                msiActual = Metricas.MSI(topologia, capacidad);                    
                resultado = 100 - ((redondearDecimales(msiActual, 6) * 100)/redondearDecimales(msiGrafo, 6));
            }
            */
            return resultado;
        }
        
        public static double redondearDecimales(double valorInicial, int numeroDecimales) {
            double resul = valorInicial * Math.pow(10, numeroDecimales);
            resul = Math.round(resul);
            resul = Math.floor(resul);
            resul = resul / (Math.pow(10, numeroDecimales));

            return resul;
        }
        
        
        public static double BFRdeRuta(GraphPath<Integer, Link> ruta, int capacidad, Graph<Integer, Link> G, EstablishedRoute resultados){
            double contSeguido = 0, mayorSeguido = 0, contOcupados;
            double sumaMaxBlocks = 0;
            int cantEnlaces = 0;
            int h=-1;
            for (Link n : ruta.getEdgeList()) {
                //la h seria el indice del vector PathCores que contiene el core en la cual fue colocada dicho enlace,
                //al pasar al otro enlace tambien la h aumenta de esa forma en la siguiente posicion del array PathCores contiene el core del segundo enlace 
                h++;  
                cantEnlaces++;
                contOcupados = 0;
                mayorSeguido=0;
                contSeguido=0;
                //for(Nodo n=ruta.getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                 //   if (G.getEdge(n.getTo(),n.getFrom()).getCores().get(h)!=null){
                        for (int k=0; k<capacidad; k++){
                            //1= libre 0 = Ocupado
                            if(G.getEdge(n.getFrom(), n.getTo()).getCores().get(resultados.getPathCores().get(h)).getFrequencySlots().get(k).isFree()){
                                
                                contSeguido++; 
                            }else{
                                if (contSeguido>mayorSeguido){
                                    mayorSeguido = contSeguido;
                                }
                                contSeguido = 0;
                                contOcupados++;
                            } 
                        }
                        if (contSeguido>mayorSeguido){
                            mayorSeguido = contSeguido;
                        }
                        if (contOcupados != capacidad){
                            sumaMaxBlocks = sumaMaxBlocks + (double) (1 - (mayorSeguido/(capacidad-contOcupados)));
                            if((1 - (mayorSeguido/(capacidad-contOcupados)))<0){
                                System.out.println("");
                            }
                        }
                  // } 
               // }
            }  
            if (sumaMaxBlocks/cantEnlaces<0){
                System.out.println("");
            }
            return (sumaMaxBlocks/cantEnlaces); 
        }
    
    //Metodo que ordena el vector de probabilidades de forma creciente
    //reordenando tambien los vectores de feromonas y visibilidad, y el array de rutas.
    public static void ordenarProbabilidad(double[] probabilidad, ArrayList<Integer> orden){
        double auxp;
        int auxi;
        int n = probabilidad.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (probabilidad[i] > probabilidad[j]) {
                    auxp = probabilidad[i];
                    probabilidad[i] = probabilidad[j];
                    probabilidad[j] = auxp;
                    
                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i,orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }
    
    //Realiza la seleccion de padres 
    //Se selecciona el primer padre por medio de la ruleta y el segundo padre al azar "RAFA"
    public static ArrayList<Cromosoma> seleccionarPadres(ArrayList<Cromosoma> poblacionActual,double[] probabilidad){
        int intAletorio=0; int intAletorio2=0;
        ArrayList<Cromosoma> padres = new ArrayList<>();
        int indice = 0; //variable para saber la ultima pos cuando hayan con prob cero y retornar uno de esos, -1 nunca debería enviar
        ArrayList<Integer> indicesProbab = new ArrayList<>();//se guardan los que ya fueron elegidos en la ruleta
        for (int i=0; i<poblacionActual.size(); i++){
            indicesProbab.add(-1);
        }
        while(padres.size()<1){
            //sumar todas las probabilidades que siguen en juego
            double sumaProbParticipan = 0;
            int n = probabilidad.length;
            for (int i = 0; i <= n - 1; i++) {
                if(!isInList(indicesProbab, i)){
                   sumaProbParticipan = sumaProbParticipan + probabilidad[i];
                }
            }       
            //hallar el valor random entre 0 a el valor máximo de probabilidades en juego
            Random randomGenerator = new Random();
            double randomValue = sumaProbParticipan * randomGenerator.nextDouble();
            double sumaProb = 0;
            indice = -1;
            while(sumaProb <= randomValue){
                indice++;
                if(indice>=probabilidad.length){
                    System.out.println();
                }
                if(!isInList(indicesProbab, indice)){
                    sumaProb = sumaProb + probabilidad[indice];
                }
            }
            if (indice >= probabilidad.length || indice < 0){
                System.out.println("oh ooh, mando índice: " + indice + ", máximo1: " + probabilidad.length);
            }
            padres.add(poblacionActual.get(indice));
        }
        Random aleatorio2 = new Random(System.currentTimeMillis());
        //while(padres.size()<2){
        intAletorio2 = aleatorio2.nextInt(poblacionActual.size());
           // if(!isInList(generadosAux,intAletorio2)){
               padres.add(poblacionActual.get(intAletorio2));
            //}
        //}
        
        return padres;
    }
    
    //Realiza el cruce entre dos padres generando dos hijos
    //Aqui se repite el proceso de verificar que cumplan con el porcentaje de mejora requido
    public static ArrayList<Cromosoma> CrucePadres(ArrayList<Cromosoma> padres,Integer rutasActivas){
        int puntoCruce1=0,puntoCruce2=0;
        Cromosoma hijo1 = new Cromosoma();
        Cromosoma hijo2 = new Cromosoma();
        ArrayList<Cromosoma> hijos = new ArrayList<Cromosoma>();
        //Generar un punto de cruce aleatorio para cada cromosoma 
        Random aleatorio1 = new Random(System.currentTimeMillis());
        Random aleatorio2 = new Random(System.currentTimeMillis());
        // Producir nuevo int aleatorio entre 0 y la longitud de cada cromosoma
        while(puntoCruce1==0){
             puntoCruce1 = aleatorio1.nextInt(padres.get(0).getListaGenes().size()-1);
        }
        while(puntoCruce2==0){
            puntoCruce2 = aleatorio2.nextInt(padres.get(1).getListaGenes().size()-1);
        }
        //**********Hijo uno**********//
        for(int i=0;i<puntoCruce1;i++){//agregamos la primera mitad del primer padre
            hijo1.getListaGenes().add(padres.get(0).getListaGenes().get(i));
        }
        for(int i=puntoCruce2+1;i<padres.get(1).getListaGenes().size();i++){//agregamos la mitad del 2do padre
            //verifica que la segunda mitad del 2do padre no tenga genes repetidos en la primera mitad del 1er padre
            if(!isInList(hijo1.getListaGenes(),padres.get(1).getListaGenes().get(i))){
               hijo1.getListaGenes().add(padres.get(1).getListaGenes().get(i));  
            }//else{//si el gen esta repetido se reemplaza aleatoriamente
                //System.out.println("Gen repetido reemplazar");
               /* Random aleatorioGen = new Random(System.currentTimeMillis());
                int genRelleno = aleatorioGen.nextInt(rutasActivas);
                while(!isInList(hijo1.getListaGenes(),genRelleno)){
                    hijo1.getListaGenes().add(genRelleno);
                }*/
            //}
        }
        hijos.add(hijo1);
        //**********Hijo 2**********//
        for(int i=puntoCruce1+1;i<padres.get(0).getListaGenes().size();i++){//agregamos la 2da mitad del primer padre
            hijo2.getListaGenes().add(padres.get(0).getListaGenes().get(i));
        }
        for(int i=0;i<puntoCruce2;i++){//agregamos la mitad del primer padre
            //verifica que la primera mitad del 2do padre no tenga genes repetidos en la 2da mitad del 1er padre
           if(!isInList(hijo2.getListaGenes(),padres.get(1).getListaGenes().get(i))){
               hijo2.getListaGenes().add(padres.get(1).getListaGenes().get(i));  
            }//else{//si el gen esta repetido se reemplaza aleatoriamente
               // System.out.println("Gen repetido reemplazar");
               /* Random aleatorioGen = new Random(System.currentTimeMillis());
                int genRelleno = aleatorioGen.nextInt(rutasActivas);
                while(!isInList(hijo1.getListaGenes(),genRelleno)){
                    hijo1.getListaGenes().add(genRelleno);
                }*/
            //}
        }
        hijos.add(hijo2);
        return hijos;
    }
    
    //Clasifica el mejor de dos individuos en base al fitnes
   /* public static Cromosoma elMejordeDos(Cromosoma cr1,Cromosoma cr2){
        if(cr1.getMejora()<cr2.getMejora()){
            return cr1;
        }else{
            return cr2;
        }
    }*/
    
    //Evaluar Cromosomas candidatos a formar parte de la población
    public static Cromosoma EvaluarCromosoma (Cromosoma hijo, List<GraphPath<Integer, Link>> rutas,
        Graph<Integer, Link> topologia ,Graph<Integer, Link> G ,int capacidadFSporEnlace,ArrayList<EstablishedRoute> resultados,
        String algoritmoAejecutar,double mejora,String objetivoAG,double mejorIndPobAct,int tamCromosoma,
        int nroGeneracion,int tamPoblacionTemp, Integer CantCores, BigDecimal MaxCrosstalk, Double crosstalkPerUnitLength, TopologiesEnum topology, BigDecimal fsWidth) throws IOException{
        
        ArrayList<Integer> indicesElegidas = new ArrayList<>(); //guarda los indices de las rutas representadas por cada gen del cromosoma
        ArrayList<GraphPath<Integer, Link>> rutasElegidas = new ArrayList<>(); //guarda las rutas del cromosoma
        ArrayList<Integer> rutasAux = new ArrayList<>(); //guarda las rutas del cromosoma
        int intAletorio;
        boolean porBfr = false,porMsi = false;
        double  bfrGrafo = 0,msiGrafo=0,mejor=0;
        double mejoraActual=0;
        int cantReruteosIguales = 0,cont=0;
        EstablishedRoute rparcial;
        
        Graph<Integer, Link> copiaGrafo = Utils.createTopology(topology,
                            CantCores,fsWidth , capacidadFSporEnlace);
        
        
        //Copia Backup =  new Copia(topologia);
        //Graph<Integer, Link> copiaGrafo;
        
       // Copia Backup =  new Copia(topologia);
       Graph<Integer, Link> grafoMejor = Utils.createTopology(topology,
                            CantCores,fsWidth , capacidadFSporEnlace); 
       
       //Graph<Integer, Link> grafoMejor;
        
        //GrafoMatriz copiaGrafo =  new GrafoMatriz(G.getCantidadDeVertices());
        //copiaGrafo.insertarDatos(topologia);
        
        //GrafoMatriz grafoMejor =  new GrafoMatriz(G.getCantidadDeVertices());
        //grafoMejor.insertarDatos(topologia);
        ArrayList<EstablishedRoute> resultadosActualElegidas = new ArrayList<>();
        ArrayList<EstablishedRoute> resultadosMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultados
        ArrayList<GraphPath<Integer, Link>> rutasMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultado
        ArrayList<Integer> indicesMejor = new ArrayList<>(); //arrayList que guarda los indices de las rutas que consiguieron la mejor solucion
        int cantidadRutasMejor=rutas.size();
        Cromosoma cr = new Cromosoma();
        boolean grafoInicial = false;
        int sumCantLink= 0;
        //Selecciona el objetivo del algoritmo AG
        switch (objetivoAG) {
              case "BFR":
                    porBfr = true;
                    bfrGrafo = Metricas.BFR(G, capacidadFSporEnlace, CantCores);
                    sumCantLink=0;
                    break;
                    /*
                case "MSI":
                    porBfr = false;
                    porMsi = true;
                    msiGrafo = Metricas.MSI(G, capacidadFSporEnlace);
                    break;
*/
        }
       for(int i=0;i<hijo.getListaGenes().size();i++){
        indicesElegidas.add(hijo.getListaGenes().get(i));
        rutasElegidas.add(rutas.get(indicesElegidas.get(i)));
       }
       cantReruteosIguales = 0;
       //Crear la copia del grafo original manualmente
       copiarGrafo(copiaGrafo, topologia, capacidadFSporEnlace);
      
       for(int j=0; j<rutasElegidas.size();j++){
       Utils.deallocateFs(copiaGrafo,resultados.get(indicesElegidas.get(j)), crosstalkPerUnitLength);
       }
       //desasignarFS_DefragProAct(rutasElegidas, resultados, copiaGrafo, indicesElegidas); //desasignamos los FS de las rutas a reconfigurar                
       //ORDENAR LISTA
       if (rutasElegidas.size()>1){
        ordenarRutas(resultados, rutasElegidas, indicesElegidas, rutasElegidas.size());
       }
       //volver a rutear con las nuevas condiciones mismo algoritmo
        int contBloqueos =0;
        resultadosActualElegidas.clear();
      for (int i=0; i<rutasElegidas.size(); i++){
        
            //int fs = resultados.get(indicesElegidas.get(i)).getFin() - resultados.get(indicesElegidas.get(i)).getInicio();
            //fs++;
            //int tVida = G.acceder(rutas.get(indicesElegidas.get(i)).getInicio().getDato(),rutas.get(indicesElegidas.get(i)).getInicio().getSiguiente().getDato()).getFS()[resultados.get(indicesElegidas.get(i)).getInicio()].getTiempo();
            //Demanda demandaActual = new Demanda(rutasElegidas.get(i).getInicio().getDato(), obtenerFin(rutasElegidas.get(i).getInicio()).getDato(), fs, tVida);
            //ListaEnlazada[] ksp = listaKSP.get(indicesElegidas.get(i));
            //rparcial = realizarRuteo(algoritmoAejecutar,demandaActual,copiaGrafo, ksp,capacidadFSporEnlace);
            int fs = resultados.get(indicesElegidas.get(i)).getFsWidth();
            fs++;
            //ver esta parte, no se si esta bien
            int tVida= resultados.get(indicesElegidas.get(i)).getLifetime();
            int insertionTime=resultados.get(indicesElegidas.get(i)).getInsertionTime();
            int idDemanda= resultados.get(indicesElegidas.get(i)).getId();
            Demand demandaActual= new Demand(idDemanda, rutasElegidas.get(i).getStartVertex(), rutasElegidas.get(i).getEndVertex(), fs, tVida, false, insertionTime);
          //  GraphPath<Integer, Link> ksp = listaKSP.get(indicesElegidas.get(i));
           // GraphPath<Integer, Link> ksp = resultados.get(indicesElegidas.get(i)).getListaKSP().get(i);
            
            rparcial= Algorithms.ruteoCoreMultiple(copiaGrafo, demandaActual, capacidadFSporEnlace, CantCores, MaxCrosstalk, crosstalkPerUnitLength);
            if (rparcial != null) {
                //    asignarFS_Defrag(ksp, rparcial, copiaGrafo, demandaActual, 0);
                Utils.assignFs(copiaGrafo, rparcial, crosstalkPerUnitLength);
                resultadosActualElegidas.add(rparcial); //guardar el conjunto de resultados para esta solucion parcial
                //verificar si eligio el mismo camino y fs para no sumar en reruteadas
                if (compararRutas(rparcial,resultados.get(indicesElegidas.get(i)))){
                    cantReruteosIguales++;
                }
            } else {
                contBloqueos++;
            }
        }
        //si hubo bloqueo no debe contar como una solucion
        if(contBloqueos==0){
            mejoraActual = Utils.calculoMejoraAG(porMsi,porBfr,copiaGrafo,bfrGrafo,capacidadFSporEnlace,msiGrafo, CantCores);

        } else {
            mejoraActual = 0;
        }
        if(mejoraActual>0.0 && hijo.getListaGenes().size()<=tamCromosoma){
            hijo.setMejora(mejoraActual);
            hijo.getListaGenes().clear();
            for(int i= 0;i<indicesElegidas.size(); i++){
                hijo.getListaGenes().add(indicesElegidas.get(i));
            } 
            cr.setMejora(mejoraActual);
            cr.setListaGenes(hijo.getListaGenes());
            cr.setNroReconf(hijo.getListaGenes().size()-cantReruteosIguales);
            //si es mejor que el mejor individuo guardar valores para graficar
            if(hijo.getMejora()> mejorIndPobAct){//entonces es mejor que el mejor individuo actual
                mejor = mejoraActual;
                cantidadRutasMejor = resultadosActualElegidas.size()-cantReruteosIguales;
                
                copiarGrafo(grafoMejor, copiaGrafo, capacidadFSporEnlace);
                
                //grafoMejor=copiaGrafo;

                //Guarda el mejor conjunto de resultados para posteriormente cambiar en el vector resultados
                resultadosMejor.clear();
                rutasMejor.clear();
                indicesMejor.clear();
                for (int k=0; k<resultadosActualElegidas.size(); k++){
                    resultadosMejor.add(resultadosActualElegidas.get(k));
                    indicesMejor.add(indicesElegidas.get(k));
                    //rutasMejor.add(listaKSP.get(indicesElegidas.get(k))[resultadosActualElegidas.get(k).getPath()]);
                    //ver si esta bien ojo
                    //rutasMejor.add((GraphPath<Integer, Link>) resultadosActualElegidas.get(k).getPath());
                    rutasMejor.add(resultadosActualElegidas.get(k).getKspPlaced());
                }
                    //guardamos los valores en el cromosoma
                    cr.setResultadosMejor(resultadosMejor);
                    cr.setRutasMejor(rutasMejor);
                   cr.setIndicesMejor(indicesMejor);
                   cr.setGrafoMejor(grafoMejor);
                    cr.setCantidadRutasMejor(cantidadRutasMejor);
                    cr.setMejora(mejor);
                   cr.setSeLogroMejora("SI");
                    //System.out.println("grafo mejor : " +cr.getGrafoMejor());
            }else if(nroGeneracion==0 && tamPoblacionTemp==50){//si no fue mejor igual graficamos si es la primera generacion
                     mejor = mejoraActual;
                     cantidadRutasMejor = resultadosActualElegidas.size()-cantReruteosIguales;
                    copiarGrafo(grafoMejor, copiaGrafo, capacidadFSporEnlace);
                      //grafoMejor=copiaGrafo;
                    //Guarda el mejor conjunto de resultados para posteriormente cambiar en el vector resultados
                    resultadosMejor.clear();
                    rutasMejor.clear();
                    indicesMejor.clear();
                    for (int k=0; k<resultadosActualElegidas.size(); k++){
                      resultadosMejor.add(resultadosActualElegidas.get(k));
                      indicesMejor.add(indicesElegidas.get(k));
                      //rutasMejor.add((GraphPath<Integer, Link>) resultadosActualElegidas.get(k).getPath());
                       rutasMejor.add(resultadosActualElegidas.get(k).getKspPlaced());
                      //rutasMejor.add(listaKSP.get(indicesElegidas.get(k))[resultadosActualElegidas.get(k).getCamino()]);
                    }
                    //guardamos los valores en el cromosoma
                    cr.setResultadosMejor(resultadosMejor);
                    cr.setRutasMejor(rutasMejor);
                    cr.setIndicesMejor(indicesMejor);
                    cr.setGrafoMejor(grafoMejor);
                    cr.setCantidadRutasMejor(cantidadRutasMejor);
                    cr.setMejora(mejor);
                    cr.setSeLogroMejora("SI");
                }
        }else{
            //si mejora actual es 0 es porque hay bloqueo entonces se envia un cromosoma vacio "RAFA"
            cr.getListaGenes().clear();
            cr.getGenesMap().clear();
        }
        return cr;
    }
    
        //Dejar solo los 50 mejores individuos en la poblacion nueva de los 100 individuos que hay emtre poblacion actual e hijos generados y estos luego quedaran como en la poblacion actual
    //se usa en la parte de desfragmentacionAG antes de aumentar la generacion.
    public static void copiarMejoresIndividuos(ArrayList<Cromosoma> poblacionNueva){
        Cromosoma auxp;
        int n = poblacionNueva.size();
        //se ordena de mayor a menor
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (poblacionNueva.get(i).getMejora() < poblacionNueva.get(j).getMejora()) {
                    auxp = poblacionNueva.get(i);
                    poblacionNueva.set(i, poblacionNueva.get(j));
                    poblacionNueva.set(j, auxp);
                }
            }
        }
        int limite = Math.round(poblacionNueva.size()/2);
        int i=poblacionNueva.size()-1;
        
        while(poblacionNueva.size()!=limite){
            poblacionNueva.remove(i);
            i=i-1;
        }
    }
    
    
     public static void MutacionHijos(Cromosoma hijo,Integer cantRutasActivas){
        Integer puntoMut1,nuevRuta;
        //Mutar:generar un numero aleatorio para saber que gen debemos eliminar 
        //Generar un punto de mutacion aleatorio para cada cromosoma 
        Random aleatorioMut1 = new Random(System.currentTimeMillis());
        // Producir puntos de cruce aleatorio para cada cromosoma
        puntoMut1 = aleatorioMut1.nextInt(hijo.getListaGenes().size()); 
        for(int i=0;i<hijo.getListaGenes().size();i++){
            if(puntoMut1==i){//reemplazar una ruta por otra sera la mutación realizada
                boolean yo =true;
                while(yo){
                nuevRuta=aleatorioMut1.nextInt(cantRutasActivas);
                    if(!isInList(hijo.getListaGenes(),nuevRuta)){
                        hijo.getListaGenes().remove(i);
                        hijo.getListaGenes().add(nuevRuta);
                        break;
                    }
                } 
            }
        }
    }
     
     
     
    //Copiar poblacion,se realiza una copia de los individuos a la nueva población 
    public static void copiarIndividuos(ArrayList<Cromosoma> poblacionActual,ArrayList<Cromosoma> poblacionNueva){
            int c=0;
       while(poblacionNueva.size()<poblacionActual.size()){
           poblacionNueva.add(poblacionActual.get(c));
           c++;
        }
    }
    
    //(copiaGrafo);
    
     //Metodo para realizar la copia de un grafo item por item
   
    
    public static void copiarGrafo(Graph<Integer, Link> copia, Graph<Integer, Link> original, int capacidad){
        ArrayList<Integer> lista = new ArrayList<>();
        int CantCores= 7;
        BigDecimal nuevoFsWidh= new BigDecimal("12.5".toString());
       for (int i=0; i<original.vertexSet().size(); i++){
            for (int j=0 ;j<original.vertexSet().size() ; j++){
                if (!Utils.isInList(lista, j)){
                    if(original.getEdge(i, j)!=null){
                        for(int h=0;h<CantCores;h++ ){
                            for (int k=0; k<capacidad; k++){   
                                
                                FrequencySlot originalSlot = original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k);

                                FrequencySlot copiedSlot = new FrequencySlot(nuevoFsWidh);
                                
                               // System.out.println("Original Slot Address: " + System.identityHashCode(originalSlot));
                                //System.out.println("Copied Slot Address: " + System.identityHashCode(copiedSlot));

                                
                               copiedSlot.setFree(originalSlot.isFree());
                                
                               // System.out.println("Original Slot Address free: " + System.identityHashCode(originalSlot.isFree()));
                               // System.out.println("Copied Slot Address: free" + System.identityHashCode(copiedSlot.isFree()));
                                
                                
                                
                                
                                copiedSlot.setFsWidh(new BigDecimal(originalSlot.getFsWidh().toString()));
                               // System.out.println("Original Slot Address setFswidh: " + System.identityHashCode(originalSlot.getFsWidh()));
                               // System.out.println("Copied Slot Address setFsWidh: " + System.identityHashCode(copiedSlot.getFsWidh()));
                                
                                
                                copiedSlot.setLifetime(originalSlot.getLifetime());
                               // System.out.println("Original Slot Address Lifetime: " + System.identityHashCode(originalSlot.getLifetime()));
                               // System.out.println("Copied Slot Address: Lifetime" + System.identityHashCode(copiedSlot.getLifetime()));
                                
                                
                                
                                
                                copiedSlot.setCrosstalk(new BigDecimal(originalSlot.getCrosstalk().toString()));
                               // System.out.println("Original Slot Address Crosstalk: " + System.identityHashCode(originalSlot.getCrosstalk()));
                              //  System.out.println("Copied Slot Address: crosstalk" + System.identityHashCode(copiedSlot.getCrosstalk()));
                                

                                copia.getEdge(i, j).getCores().get(h).getFrequencySlots().set(k, copiedSlot);
                                
                                //System.out.println("Copia Slot Address: free" + System.identityHashCode(copia.getEdge(i, j).getCores().get(h).getFrequencySlots().set(k, copiedSlot).isFree()));
                                //System.out.println("Original Slot Address free: " + System.identityHashCode(original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).isFree()));
                              

                            }
                        } 
                    }
                }
            }            
            lista.add(i);
        }
    }
    
    
    
    /*
    public static void copiarGrafo(Graph<Integer, Link> copia, Graph<Integer, Link> original, int capacidad){
        ArrayList<Integer> lista = new ArrayList<>();
        int CantCores= 7;
        
        for(int i=0;i<original.getCantidadDeVertices();i++){
            for(int j=0;j<original.getCantidadDeVertices();j++){
                if(original.acceder(i, j)!=null){
                    for (int k=0; k<capacidad; k++){
                        //copia.acceder(i, j).getFS()[k].setEstado(original.acceder(i, j).getFS()[k].getEstado());
                        //copia.acceder(i, j).getFS()[k].setTiempo(original.acceder(i, j).getFS()[k].getTiempo());
                        //copia.acceder(i, j).getFS()[k].setConexion(original.acceder(i, j).getFS()[k].getConexion());
                        //copia.acceder(i, j).getFS()[k].setPropietario(original.acceder(i, j).getFS()[k].getPropietario());
                    } 
                }
            }
        }
        
        
       for (int i=0; i<original.vertexSet().size(); i++){
            for (int j=0 ;j<original.vertexSet().size() ; j++){
                // System.out.println("cantidad de enlaces  : "+ sumCantLink );
                if (!Utils.isInList(lista, j)){
                   //for(int h=0;h<G.getEdge(i, j).getCores().size();h++ ){
                    if(original.getEdge(i, j)!=null){
                        for(int h=0;h<CantCores;h++ ){
                            for (int k=0; k<capacidad; k++){
                            //1= libre 0 = Ocupado
                            
                               //true=libre  false=ocupado "RAFA"
                              //if (G.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).isFree()){
                              
                              boolean freeJsonObjeto= original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).isFree();
                              
                              String strfree= ConvertirJSON.freeJSON(freeJsonObjeto);
                              boolean freeObjetoJson= ConvertirObject.obtenerFree(strfree);
                              
                              
                              copia.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).setFree(freeJsonObjeto);
                              
                              BigDecimal ValorFsWidh= original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).getFsWidh();
                              copia.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).setFsWidh(ValorFsWidh);
                              
                              int ValorLifetime= original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).getLifetime();
                              copia.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).setLifetime(ValorLifetime);
                              
                              
                              BigDecimal ValorCrosstalk=original.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).getCrosstalk();
                              copia.getEdge(i, j).getCores().get(h).getFrequencySlots().get(k).setCrosstalk(ValorCrosstalk);
                            
                            }
                            //} 
                        } 
                    }
                }
            }            
            lista.add(i);
        }
    }
    
    */
    
    
    
    
    
    
    //Metodo que elige la ruta a seleccionar de acuerdo a su vector de probabilidades
    public static int elegirRuta(double[] p, ArrayList<Integer> indices, ArrayList<Integer> indexOrden){
        //System.out.println("Inicia metodo de Ruleta ");
        int indice;
        int ultPosProbCero = -1; //variable para saber la ultima pos cuando hayan con prob cero y retornar uno de esos, -1 nunca debería enviar
        ArrayList<Integer> indicesProbab = new ArrayList<>();
        for (int i=0; i<indices.size(); i++){
            indicesProbab.add(indexOrden.indexOf(indices.get(i)));
        }
        //sumar todas las probabilidades que siguen en juego
        double sumaProbParticipan = 0;
        //p seria el array que contiene las probabilidad de cada ruta activa, calculado mediante el bfr
        int n = p.length;
        for (int i = 0; i <= n - 1; i++) {
            if(!isInList(indicesProbab, i)){
                sumaProbParticipan = sumaProbParticipan + p[i];
                if (p[i] == 0){
                    ultPosProbCero = i;
                }
            }
        }
        
        //si ya solo quedan opcines con prob cero, le envio el ultimo que no se eligió aún con prob cero
        if(sumaProbParticipan == 0){
            return ultPosProbCero;
        }
        
        //hallar el valor random entre 0 a el valor máximo de probabilidades en juego
        Random randomGenerator = new Random();
        double randomValue = sumaProbParticipan * randomGenerator.nextDouble();

        double sumaProb = 0;
        indice = -1;
        while(sumaProb <= randomValue){
            //System.out.println("probar randomValue: " + randomValue+ ", sum: " + sumaProb);
            indice++;
            if(indice>=p.length){
                System.out.println();
            }
            if(!isInList(indicesProbab, indice)){
                sumaProb = sumaProb + p[indice];
            }
        }
        
        if (indice >= p.length || indice < 0){
            System.out.println("indice: " + indice+ ", p.length: " + p.length);
            System.out.println("oh ooh, mando índice: " + indice + ", máximo2: " + p.length);
        }
        return indice;
    }

}
